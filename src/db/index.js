import idb from 'idb'
const dbPromised = idb.open('favorites', 1, function(upgradeDb) {
  const teamsObjectStore = upgradeDb.createObjectStore("teams", {
    keyPath: "id"
  });
  teamsObjectStore.createIndex("name", "name", { unique: false });
});

export const saveTeam = function (team) {
 return new Promise(function(resolve, reject) {
    dbPromised
    .then(function(db) {
      const tx = db.transaction("teams", "readwrite");
      const store = tx.objectStore("teams");
      console.log('akan melakukan penyimpanan team favorite', team);
      store.put(team);
      return tx.complete;
    })
    .then(function(d) {
      resolve(d)
      console.log("favorite team berhasil di simpan.");
    });
  });
}

export const getTeams = function() {
  return new Promise(function(resolve, reject) {
    dbPromised
      .then(function(db) {
        const tx = db.transaction("teams", "readonly");
        const store = tx.objectStore("teams");
        return store.getAll();
      })
      .then(function(teams) {
        resolve(teams);
      });
  });
}

export const getTeam = function(id) {
  id = Number(id)
  return new Promise(function(resolve, reject) {
    dbPromised
      .then(function(db) {
        const tx = db.transaction("teams", "readonly");
        const store = tx.objectStore("teams");
        return store.get(id);
      })
      .then(function(team) {
        resolve(team);
      });
  });
}
export const deleteTeam = function(id) {
  id = Number(id)
  return new Promise(function(resolve, reject) {
    dbPromised
      .then(function(db) {
        const tx = db.transaction("teams", "readwrite");
        const store = tx.objectStore("teams");
        store.delete(id);
        return tx.complete
      })
      .then(function(d) {
        window.console.log('sukes menghapus teams')
        resolve(d)
      });
  });
}