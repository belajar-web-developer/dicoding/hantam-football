const navs = [
  {
    icon: 'home',
    target: 'home',
    label: 'Home',
  },
  {
    icon: 'explore',
    target: 'teams',
    label: 'Team',
  },
  {
    icon: 'favorite',
    target: 'favorite',
    label: 'Favorit',
  },
];
export default {
  template: `
  <nav class="nav-extended cyan darken-4">
    <div class="nav-wrapper  container">
      <a href="#" class="brand-logo">Hantam<b>Fotball</b></a>
      <ul id="nav-lists" class="right hide-on-med-and-down">
      </ul>
    </div>
    <div class="nav-content container" id="nav-tabs">
    </div>
  </nav>
  <div id="app-main"></div>
  <div class="bottom-nav hide-on-large-only">
      <nav id="nav-lists-bottom">
      </nav>
  </div>
  `,
  mounted () {
    navs.forEach((item) => {
      window.$el("#nav-lists").innerHTML += `<li><router-link to="${item.target}">${item.label}</router-link></li>`
      window.$el("#nav-lists-bottom").innerHTML += `
        <router-link to="${item.target}" class="bottom-nav-item waves-effect">
            <span class="material-icons bottom-nav-item-icon">${item.icon}</span>
            <span class="bottom-nav-item-text">${item.label}</span>
        </router-link>`
    })
    
  }
}