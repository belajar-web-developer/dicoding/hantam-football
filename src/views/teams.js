import { baseUrl, headers } from '~/config'
import '~/imgs/default-logo.png'
export default {
  template: '<div class="container"><div class="row" id="teams-container"></div><div>',
  mounted () {
    // loader show
    const mainContainer = `#teams-container`
    window.$showLoading(mainContainer).show()

    // get data team liga champions //
    fetch(baseUrl + 'competitions/2001/teams',{
      headers: headers,
    })
    .then(response => response.json())
    .then(data => {
      if (!window.$el(mainContainer)) {return false}
      window.$showLoading(mainContainer).hide()

      const {teams} = data
      window.console.log(teams)
      teams.forEach((team) => {
        window.$el(mainContainer).innerHTML += `
          <div class="col s6 m3">
            <router-link to="detailteam" data-id="${team.id}">
              <div class="card-panel card-team center">
                <div>
                  <img onerror="this.onerror=null; this.src='./imgs/default-logo.png'" alt="${team.shortName}" src="${team.crestUrl ? team.crestUrl.replace("http://", "https://") : './imgs/default-logo.png'}" class="logo-bendera">
                </div>
                <div>
                  ${team.shortName}
                </div>
              </div>
            </router-link>
          </div>
        `
      })
    })
  }
}