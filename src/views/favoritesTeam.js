import { baseUrl, headers } from '~/config'
import '~/imgs/default-logo.png'
import { getTeams } from '~/db'
export default {
  template: '<div class="container"><div class="row" id="favorite-container"></div><div>',
  mounted () {
    // loader show
    const mainContainer = `#favorite-container`
    window.$showLoading(mainContainer).show()

    // get data team favorite       //
    getTeams().then(teams => {
      if (!window.$el(mainContainer)) {return false}
      window.$showLoading(mainContainer).hide()
      window.console.log(teams)
      teams.forEach((team) => {
        window.$el(mainContainer).innerHTML += `
          <div class="col s6 m3">
            <router-link to="detailteam" data-id="${team.id}">
              <div class="card-panel card-team center">
                <div>
                  <img alt="${team.shortName}" onerror="this.onerror=null; this.src='./imgs/default-logo.png'"  src="${team.crestUrl ? team.crestUrl.replace("http://", "https://") : './imgs/default-logo.png'}" class="logo-bendera">
                </div>
                <div>
                  ${team.shortName}
                </div>
              </div>
            </router-link>
          </div>
        `
      })

      if (teams.length == 0) {
        window.$el(mainContainer).innerHTML = `
          <div class="col s12 center">
              <br/>
              <br/>
              <i class="material-icons large">sentiment_dissatisfied</i>
              <h5>
                  Kamu tidak mempunyai data team favorite
              </h5>
          </div>
        `
      }

    })
  }
}