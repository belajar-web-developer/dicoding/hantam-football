import { baseUrl, headers } from '~/config'
import { saveTeam, getTeam, deleteTeam } from '~/db'
import { toast } from '~/helpers'

function changeFavButton(type, data) {
  if (type == 'save') {
    window.$el("#saveTeam").innerHTML = `<a href="javascript:void(0)" id="saveTeamBtn"><i class="material-icons">favorite_border</i></a>`
    window.$el("#saveTeamBtn").addEventListener("click", function(event) {
      saveTeam(data).then((d) => {
        toast('Sukses menyimpan favorit', 'cyan darken-4')
        changeFavButton('delete', data)
      })
    })
  } else {
    window.$el("#saveTeam").innerHTML = `<a href="javascript:void(0)" id="deleteTeamBtn"><i class="material-icons">favorite</i></a>`
    window.$el("#deleteTeamBtn").addEventListener("click", function(event) {
      deleteTeam(data.id).then((d) => {
        toast('Sukses menghapus favorit', 'cyan darken-4')
        changeFavButton('save', data)
      })
    })
  }
}

export default {
  template: `
  <nav class="cyan darken-4">
    <div class="nav-wrapper container">
      <router-link to="teams" style="margin-right: 10px" class="left"><i class="material-icons">chevron_left</i></router-link>
      <a href="#" class="brand-logo">Hantam<b>Fotball</b></a>
      <ul id="nav-mobile" class="right">
        <li id="saveTeam"></li>
      </ul>
    </div>
  </nav>
  <div class="container" id="main-content">
  </div>
  `,
  async mounted () {
    window.$showLoading('#main-content').show()

    const params = window.$getParams(window.location.href)
    window.console.log('parameter url sekarang', params)

    let data = await getTeam(params.id)
    
    if (!data) {
      const response = await fetch(baseUrl + `teams/${params.id}`,{
        headers: headers,
      });
      data = await response.json()
      changeFavButton('save', data)
    } else {
      changeFavButton('delete', data)
    }

    window.console.log(data)
    // window.$showLoading('#main-content').hide()
    window.$el("#main-content").innerHTML = `
    <ul class="collection with-header">
      <li class="collection-header">
        <h5>Informasi Team</h5>
      </li>
      <li class="collection-item">
        <div class="row">
          <div class="col s4">
            <img alt="${ data.shortName }" onerror="this.onerror=null; this.src='./imgs/default-logo.png'" src="${data.crestUrl ? data.crestUrl.replace("http://", "https://") : './imgs/default-logo.png'}" class="responsive-img">
          </div>
          <div class="col s8 right-align">
            <h5>
              <strong>${ data.shortName }</strong>
            </h5>
          </div>
        </div>
      </li>
      <li class="collection-item">
        <span>Nama Lengkap</span>
        <span class="secondary-content">${ data.name }</span>
      </li>
      <li class="collection-item">
        <span>No. Telp</span>
        <span class="secondary-content">${ data.phone }</span>
      </li>
      <li class="collection-item">
        <span>Website</span>
        <span class="secondary-content">${ data.website }</span>
      </li>
      <li class="collection-item">
        <span>Area</span>
        <span class="secondary-content">${ data.area.name }</span>
      </li>
      <li class="collection-item">
        <span>Lokasi</span>
        <span class="secondary-content">${ data.venue }</span>
      </li>
      <li class="collection-item">
        <span>Alamat</span>
        <span class="secondary-content">${ data.address }</span>
      </li>
    </ul>

    <ul class="collection with-header" id="formasi-team">
      <li class="collection-header">
        <h5>Squad</h5>
      </li>
    </ul>
    `

    data.squad.forEach((item) => {
      window.$el("#formasi-team").innerHTML += `
      <li class="collection-item avatar">
        <i class="material-icons circle teal">account_circle</i>
        <span class="title">${item.name} (${item.shirtNumber})</span>
        <table>
          <tr>
            <td>Posisi</td>
            <td>: ${item.position}</td>
          </tr>
          <tr>
            <td>Kebangsaan</td>
            <td>: ${item.nationality}</td>
          </tr>
        </table>
      </li>
      `
    })
  }
}