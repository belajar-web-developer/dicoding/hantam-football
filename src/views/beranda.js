import { baseUrl, headers } from '~/config'
import '~/imgs/default-logo.png'

function renderCards(el, table) {
  table.forEach((team) => {
    window.$el(el).innerHTML += `
    <div class="col s12 m6">
      <router-link to="detailteam" data-id="${team.team.id}">
        <div class="card-panel card-team">
          <div class="row">
            <div class="logo col s6 center">
              <img alt="${team.team.name}" onerror="this.onerror=null; this.src='./imgs/default-logo.png'" src="${team.team.crestUrl ? team.team.crestUrl.replace("http://", "https://") : './imgs/default-logo.png'}" class="logo-bendera">
            </div>
            <div class="detail col s6 center">
              <h6><b>${team.team.name}</b></h6>
            </div>
            <div class="col s12">
              <table class="table-match">
                <thead>
                  <tr>
                    <th>Posisi</th>
                    <th>Permainan</th>
                    <th>Menang</th>
                    <th>Imbang</th>
                    <th>Kalah</th>
                    <th>Point</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>${team.position}</td>
                    <td>${team.playedGames}</td>
                    <td>${team.won}</td>
                    <td>${team.draw}</td>
                    <td>${team.lost}</td>
                    <td>${team.points}</td>
                  </tr>
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </router-link>
    </div>
    `;
  })
}

export default {
  template: `
  <div class="container">
    <div id="standings-container">
      <div class="row" id="total">
      </div>
      <div class="row" id="home">
      </div>
      <div class="row" id="away">
      </div>
    </div>
  <div>`,
  mounted () {
    // membuat menu tabs materialize
    window.$el("#nav-tabs").innerHTML = `
    <ul class="tabs tabs-transparent" id="tabs-standings">
      <li class="tab"><a href="#total">Total</a></li>
      <li class="tab"><a href="#home">Home</a></li>
      <li class="tab"><a href="#away">Away</a></li>
    </ul>`
    M.Tabs.init(window.$el('#tabs-standings'));

    ['total', 'home', 'away'].forEach(item => window.$showLoading(`#standings-container #${item}`).show())

    fetch(baseUrl + 'competitions/2001/standings',{
      headers: headers,
    })
    .then((response) => response.json())
    .then((data) => {
      if (!window.$el("#standings-container")) { return false }
      ['total', 'home', 'away'].forEach(item => window.$showLoading(`#standings-container #${item}`).hide())

      const {standings} = data
      window.console.log(standings.filter((x) => x.type == 'TOTAL'));
      ['TOTAL', 'HOME', 'AWAY'].forEach((type) => {
        const  groups = standings.filter((x) => x.type == type); 
        groups.forEach((group) => {
          window.$el(`#${type.toLowerCase()}`).innerHTML += `<div class="col s12"><h6>${group.group.replace(/_/gi, ' ')}</h6></di>`
          renderCards(`#${type.toLowerCase()}`, group.table)
        })
      })

    })
  }
}