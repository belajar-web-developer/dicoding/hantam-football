class MyElement extends HTMLElement {
 render() { // (1)
    // const to = JSON.parse(decodeURI(this.getAttribute('props')))
    const to = this.getAttribute('to')
    const params = this.dataset
    this.addEventListener("click", function(event) {
      window.console.log('router link di klik',to, params)
      let hasilParam = '?'
      Object.keys(params).forEach(param => hasilParam += `${param}=${params[param]}&` )
      // window.console.log(hasilParam)
      window.$loadPage(to)
      window.location.href = `#${to}${hasilParam}`

    });
  }

  connectedCallback() {
    this.render()
  }
}

customElements.define("router-link", MyElement);