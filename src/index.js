import 'regenerator-runtime'

if ("serviceWorker" in navigator) {
  window.addEventListener("load", function() {
    navigator.serviceWorker
      .register("service-worker.js", {
        scope: '.'
      })
      .then(function() {
        console.log("Pendaftaran ServiceWorker berhasil");
      })
      .catch(function() {
        console.log("Pendaftaran ServiceWorker gagal");
      });
  });
} else {
  console.log("ServiceWorker belum didukung browser ini.");
}

// request notifikasi
if ('Notification' in window) {
  Notification.requestPermission().then(function (result) {
    if (result === "denied") {
      console.log("Fitur notifikasi tidak diijinkan.");
      return;
    } else if (result === "default") {
      console.error("Pengguna menutup kotak dialog permintaan ijin.");
      return;
    }
    
    navigator.serviceWorker.ready.then(() => {
      if (('PushManager' in window)) {
          navigator.serviceWorker.getRegistration().then(function(registration) {
              registration.pushManager.subscribe({
                  userVisibleOnly: true,
                  applicationServerKey: urlBase64ToUint8Array('BP-b7zmL3tjT6_xkc1AiJcs9KtZmzfqrJUuu11iXjHKMR9ZBZG_AyfZ8wpaZIfWoFvMOaZrnVa4imrrjlnX18Xo')
              }).then(function(subscribe) {
                  console.log('Berhasil melakukan subscribe dengan endpoint: ', subscribe.endpoint);
                  console.log('Berhasil melakukan subscribe dengan p256dh key: ', btoa(String.fromCharCode.apply(
                      null, new Uint8Array(subscribe.getKey('p256dh')))));
                  console.log('Berhasil melakukan subscribe dengan auth key: ', btoa(String.fromCharCode.apply(
                      null, new Uint8Array(subscribe.getKey('auth')))));
              }).catch(function(e) {
                  console.error('Tidak dapat melakukan subscribe ', e.message);
              });
          });
      }
    });
  });
}



import '~/components'
// install icon material design
import 'material-design-icons/iconfont/material-icons.css'
// materialize framework
import 'materialize-css/dist/css/materialize.min.css'
import 'materialize-css'

// my styles
import '~/styles/style.sass'
// import helper urlBase64ToUint8Array untuk registrasi service worker
import { urlBase64ToUint8Array } from './helpers/index.js'
import '~/helpers/mix.js'
// improt router
import './router'

