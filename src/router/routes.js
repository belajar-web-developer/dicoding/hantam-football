import layoutBeranda from '~/layouts/beranda'
import home from '~/views/beranda'
import teams from '~/views/teams'
import favoritesTeam from '~/views/favoritesTeam'
import detailteam from '~/views/detailTeam'

const routes = [
  {
    url: 'home',
    layout: layoutBeranda,
    view: home
  },
  {
    url: 'teams',
    layout: layoutBeranda,
    view: teams
  },
  {
    url: 'favorite',
    layout: layoutBeranda,
    view: favoritesTeam
  },
  {
    url: 'detailteam',
    view: detailteam
  }
]
export default routes