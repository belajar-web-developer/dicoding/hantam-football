import routes from  './routes'
import page404 from '~/views/404'

// system loader
window.$loadPage = function(target) {
  const page = target.split('?')[0];
  window.console.log('page akan diload', page)
  const cekRoutesIndex = routes.findIndex((x) => x.url == page)
  if (cekRoutesIndex < 0) {
    window.$el('#app').innerHTML = page404.template
  } else {
    const route = routes[cekRoutesIndex]

    if (route.layout) {
      if (route.layout.created) {
        route.layout.created()
      }

      window.$el("#app").innerHTML = route.layout.template

      if (route.layout.mounted) {
        route.layout.mounted()
      }
    }

    if (route.view.created) {
      route.view.created()
    }
    window.$el(route.layout ? '#app-main' : '#app').innerHTML = route.view.template

    if (route.view.mounted) {
      setTimeout(() => {
        route.view.mounted()
      }, 500)
    }
  }
}


// lakukan pemangggilan pertama kali ketika selesai di load //
let page = window.location.hash.substr(1);
if (page == "") page = "home";
window.$loadPage(page)