const path = require("path"); // untuk penyesuaian path
const HtmlWebpackPlugin = require("html-webpack-plugin"); // untuk generator html
const WebpackPwaManifest = require('webpack-pwa-manifest');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { InjectManifest } = require("workbox-webpack-plugin")
module.exports = {
    entry: {
      main: "./src/index.js",
      // 'service-worker': './src/service-worker.js'
    },
    output: {
        chunkFilename: '[name].bundle.js',
        filename: '[name].bundle.js', // the file name would be my entry's name with a ".bundle.js" suffix
        path: path.resolve(__dirname, 'dist') // put all of the build in a dist folder
    },
    resolve: {
      alias: {
        '~': path.resolve(__dirname, 'src/'),
      }
    },
    optimization: {
      splitChunks: {
        chunks: 'all',
      },
    },
    module: {
        rules: [
        {
            test: /\.css$/,
            use: [
                {
                    loader: "style-loader"
                },
                {
                    loader: "css-loader"
                }
            ]
        },
        {
            test: /\.s[ac]ss$/i,
            use: [
                'style-loader',
                'css-loader',
                'sass-loader',
            ],
        },
        {
          test: /\.(png|jpg|svg)$/,
          loader: 'file-loader',
          options: {
            name: 'imgs/[name].[ext]'
          }
        },
        {
          test: /\.(ttf|eot|woff|woff2)$/,
          loader: 'file-loader',
          options: {
            name: 'fonts/[name].[ext]'
          }
        }
      ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            filename: "index.html",
            chunks: ['main'],
            cache: true,
            meta: {
              viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no',
              themeColor: '#00695c',
            }
        }),
        new FaviconsWebpackPlugin({
            logo: './src/imgs/default-logo.png',
            cache: true,
            inject: true,
            favicons: {
              appName: 'Hantam Football',
              appShortName: 'Football',
              appDescription: 'Informasi Seputar Bola Terupdate',
              background: '#c4c4c4',
              theme_color: '#00695c',
              display: 'standalone',
              start_url: '/index.html',
              orientation: 'portrait',
              loadManifestWithCredentials: true,
              icons: {
                appleStartup: false,
                yandex: false,
              }
            }
        }),        
        new InjectManifest({
          swSrc: './src/service-worker.js',
          swDest: 'service-worker.js',
          include: [/\.html$/, /\.js$/, /\.css$/, /\.jpg$/, /\.png$/, /\.ico$/, /\.ttf$/, /\.eot$/, /\.woff$/, /\.woff2$/, /\.json$/],
        }),
    ]
};
